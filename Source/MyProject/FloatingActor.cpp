// Fill out your copyright notice in the Description page of Project Settings.

#include "FloatingActor.h"


// Sets default values
AFloatingActor::AFloatingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFloatingActor::BeginPlay()
{
	Super::BeginPlay();

	switch (Magnitudes) {
		case EMagnitudes::None:
			MagnitudeGain = 0.0f;
			break;
		case EMagnitudes::Low:
			MagnitudeGain = 10.0f;
			break;
		case EMagnitudes::Medium:
			MagnitudeGain = 20.0f;
			break;
		case EMagnitudes::High:
			MagnitudeGain = 50.0f;
			break;
		default:
			MagnitudeGain = 10.0f;
			break;
	}
}

// Called every frame
void AFloatingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Some variable bogus: x: %u"), (uint8)Magnitudes));

	FVector NewLocation = GetActorLocation();
	float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime));
	NewLocation.Z += DeltaHeight * MagnitudeGain;       //Scale our height by a factor of 20
	RunningTime += DeltaTime;
	SetActorLocation(NewLocation);
}

