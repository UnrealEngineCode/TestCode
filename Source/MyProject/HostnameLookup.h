// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "Runnable.h"
#include "PlatformProcess.h"
#include "Networking.h"
#include <string>
#include <algorithm>

/**
 * 
 */
class MYPROJECT_API HostnameLookup : public FRunnable
{

public:
	FThreadSafeCounter StopTaskCounter;
	static HostnameLookup* Runnable;
	FRunnableThread* Thread;
	uint32 *IP;
	const FString *LookupHostName;

	HostnameLookup(uint32 *IpNumber, const FString *Name);
	virtual ~HostnameLookup();

	// Begin FRunnable interface.
	virtual bool Init() override;
	virtual uint32 Run() override;
	virtual void Stop() override;
	// End FRunnable interface

	bool IsFinished() const
	{
		return (StopTaskCounter.GetValue() == 1);
	}

	void EnsureCompletion();

	static HostnameLookup* Lookup(uint32 *IpNumber, const FString *Name);

	static void Shutdown();

	static bool IsThreadFinished();

	uint32 GetIP();
};

