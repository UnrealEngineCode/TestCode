// Fill out your copyright notice in the Description page of Project Settings.

#include "RotatingActor.h"

// Sets default values
ARotatingActor::ARotatingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ResetCountdownTime = 2;
	RotationAngle = 20.0f;

	FlameoutCountdownScale = 1.0f;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));

	ParticleSystem = CreateDefaultSubobject<UParticleSystem>(TEXT("My EPIC PS"));
	PSC = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("My EPIC PSC"));
	auto particletemp = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Fire.P_Fire'"));
	ParticleSystem = particletemp.Object;
	PSC->SetTemplate(ParticleSystem);
	PSC->SetupAttachment(RootComponent);
	OriginalFlameTransform = GetTransform();

	StaticMesh = CreateDefaultSubobject<UStaticMesh>(TEXT("AWESOME MESH"));
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AWESOME Component MESH"));

	auto meshtemp = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Game/StarterContent/Props/SM_Lamp_Wall.SM_Lamp_Wall"));
	StaticMesh = meshtemp.Object;
	StaticMeshComponent->SetStaticMesh(StaticMesh);
	StaticMeshComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ARotatingActor::BeginPlay()
{
	OriginalTransform = StaticMeshComponent->GetComponentToWorld();
	Super::BeginPlay();
}

// Called every frame
void ARotatingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARotatingActor::AdvanceRotationTimer()
{
	//We're done counting down, so stop running the timer.
	GetWorldTimerManager().ClearTimer(ResetCountdownTimerHandle);

	//Reset the rotation to the original one
	FTransform SM_Transform = StaticMeshComponent->GetComponentToWorld();
	FQuat SM_Quat = OriginalTransform.GetRotation();
	SM_Transform.SetRotation(SM_Quat);
	StaticMeshComponent->SetWorldTransform(SM_Transform);
}

void ARotatingActor::AdvanceFlameoutTimer()
{

	FTransform CurrentTransform = PSC->GetRelativeTransform();
	
	if (FlameoutCountdownScale > 0.85f)
	{
		FlameoutCountdownScale *= 0.998f;
		CurrentTransform.SetScale3D(FlameoutCountdownScale*CurrentTransform.GetScale3D());
		PSC->SetRelativeTransform(CurrentTransform);
	}
	else {
		GetWorldTimerManager().ClearTimer(FlameCountdownTimerHandle);
		FlameoutCountdownScale = 1.0f;
		CurrentTransform.SetScale3D(0.0f*OriginalFlameTransform.GetScale3D());
		
	}
	FTransform *DisplayTransform = &CurrentTransform;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Current Scale %s !"), *DisplayTransform->ToString()));
}

void ARotatingActor::ResetCountdownHasFinished_Implementation()
{
	//Change to a special readout
}

void ARotatingActor::RotateActor()
{

	FTransform SM_Transform = StaticMeshComponent->GetComponentToWorld();
	FVector SM_Position = SM_Transform.GetLocation();
	FQuat SM_Quat = SM_Transform.GetRotation();
	FRotator SM_Rotation = SM_Quat.Rotator();
	SM_Rotation.Yaw += RotationAngle;
	StaticMeshComponent->SetWorldLocationAndRotation(SM_Position, SM_Rotation);
	
	if (ResetCountdownTimerHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(ResetCountdownTimerHandle);
	}

	GetWorldTimerManager().SetTimer(ResetCountdownTimerHandle, this, &ARotatingActor::AdvanceRotationTimer, ResetCountdownTime, true);
}

void ARotatingActor::FuelActor()
{
	FlameoutCountdownScale += 0.01;
	if (FlameoutCountdownScale < 1.0f) {
		FlameoutCountdownScale = 1.0f;
	}
	else if(FlameoutCountdownScale > 1.05f){
		FlameoutCountdownScale = 1.05f;
	}

	if (!FlameCountdownTimerHandle.IsValid()) {
		PSC->SetRelativeTransform(OriginalFlameTransform);
	}

	GetWorldTimerManager().SetTimer(FlameCountdownTimerHandle, this, &ARotatingActor::AdvanceFlameoutTimer, 0.1f, true);
}

#if WITH_EDITOR
void ARotatingActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	FName PropertyName = (PropertyChangedEvent.Property != nullptr)
		? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if (PropertyName == GET_MEMBER_NAME_CHECKED(ARotatingActor, StaticMesh))
	{
		if (StaticMesh != nullptr)
		{
			StaticMeshComponent->SetStaticMesh(StaticMesh);
		}
	}

	if (PropertyName == GET_MEMBER_NAME_CHECKED(ARotatingActor, ParticleSystem))
	{
		if (ParticleSystem != nullptr)
		{
			PSC->SetTemplate(ParticleSystem);
		}
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

void ARotatingActor::OnConstruction(const FTransform& Transform)
{
	PSC->SetWorldTransform(StaticMeshComponent->GetComponentToWorld());
	Super::OnConstruction(Transform);
}
