// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatingActor.generated.h"

UENUM(Meta = (Enum))
enum class EMagnitudes : uint8
{
	None      UMETA(DisplayName = "No Magnitude"),
	Low       UMETA(DisplayName = "Low Magnitude"),
	Medium    UMETA(DisplayName = "Medium Magnitude"),
	High      UMETA(DisplayName = "High Magnitude"),
};

UCLASS()
class MYPROJECT_API AFloatingActor : public AActor
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Meta = (Bitmask, BitmaskEnum = "EMagnitudeFlags"))
	int32 MagnitudeFlag;

	UPROPERTY(EditAnywhere, Meta = (Enum = "EMagnitudes"))
	EMagnitudes Magnitudes = EMagnitudes::None;

public:	
	// Sets default values for this actor's properties
	AFloatingActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float RunningTime;
	//fload runningJustintime=0.0;
	//here is the source
	
	float MagnitudeGain;
};

UENUM(Meta = (Bitflags))
enum class EMagnitudeFlags
{
	ECB_Low,
	ECB_Medium,
	ECB_High
};

