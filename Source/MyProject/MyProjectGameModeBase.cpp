// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProjectGameModeBase.h"

void AMyProjectGameModeBase::SetupMenus()
{
	for (auto Elem : MenuMap)
	{
		if (Elem.Value != nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("POPULATING WIDGETS"));
			MenuInstances.Add(Elem.Key, CreateWidget<UUserWidget>(GetWorld(), Elem.Value));
			VisibleMenus.Add(Elem.Key, false);
		}
	}
	ChangeMenuWidget(StartingWidgetClass);
}

void AMyProjectGameModeBase::ChangeMenuWidget(EMenuEnum MenuToDisplay)
{
	if (!MenuInstances.Contains(MenuToDisplay))
	{
		UE_LOG(LogTemp, Error, TEXT("MISSING MENU, PLEASE ADD MISSING MENU"));
		return;
	}

	UE_LOG(LogTemp, Error, TEXT("Changing Menu Widget"));
	FString ObjectName = MenuInstances.FindRef(MenuToDisplay)->GetName();
	UE_LOG(LogTemp, Warning, TEXT("CURRENT MENU WIDGET IS: %s"), *ObjectName);
	MenuInstances.FindRef(MenuToDisplay)->SetVisibility(ESlateVisibility::Visible);

	if (!MenuInstances.FindRef(MenuToDisplay)->IsInViewport())
	{
		MenuInstances.FindRef(MenuToDisplay)->AddToViewport();
		VisibleMenus.Add(MenuToDisplay) = true;
	}
}

void AMyProjectGameModeBase::RemoveMenuWidgetFromViewport(EMenuEnum MenuToRemove)
{

	if (!MenuInstances.Contains(MenuToRemove))
	{
		UE_LOG(LogTemp, Error, TEXT("MISSING MENU, PLEASE ADD MISSING MENU"));
		return;
	}

	UE_LOG(LogTemp, Error, TEXT("REMOVING WIDGET"));
	MenuInstances.FindRef(MenuToRemove)->SetVisibility(ESlateVisibility::Hidden);
	VisibleMenus.Add(MenuToRemove) = false;
}
