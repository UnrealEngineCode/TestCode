// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "RotatingActor.generated.h"

class AActor;
class UActorComponent;
class UParticleSystem;
class UParticleSystemComponent;

UCLASS()
class MYPROJECT_API ARotatingActor : public AActor
{
	GENERATED_BODY()

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
		
public:	
	// Sets default values for this actor's properties
	ARotatingActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& Transform) override;

//How long, in seconds, before reset
UPROPERTY(EditAnywhere)
int32 ResetCountdownTime;

//What angle, in degrees, should the Actor rotate
UPROPERTY(EditAnywhere)
float RotationAngle;

//The static mesh to rotate
UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = StaticMesh)
UStaticMesh* StaticMesh;

UStaticMeshComponent* StaticMeshComponent;

//Countdown timer handle for resetting actor position
FTimerHandle ResetCountdownTimerHandle;

//(Re)store the original transform 
FTransform OriginalTransform;

FTransform OriginalFlameTransform;

//Fiery particle system that burns
UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = ParticleSystem)
UParticleSystem* ParticleSystem;

UParticleSystemComponent* PSC;

//How long, in seconds, before flame out
UPROPERTY(EditAnywhere, Category = ParticleSystem)
float FlameoutCountdownScale;

//Countdown timer handle for particle system flame out
FTimerHandle FlameCountdownTimerHandle;

void AdvanceFlameoutTimer();

//Event that occurs when the countdown finished
UFUNCTION(BlueprintNativeEvent)
void ResetCountdownHasFinished();

virtual void ResetCountdownHasFinished_Implementation();

void AdvanceRotationTimer();

UFUNCTION(BlueprintCallable)
void RotateActor();

//Fuels the fiery fires
UFUNCTION(BlueprintCallable)
void FuelActor();

};
