// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "MyProjectGameModeBase.generated.h"

class AGameModeBase;

/**
 * 
 */

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EMenuEnum : uint8
{
	Default		UMETA(Hidden),
	Main_Menu 	UMETA(DisplayName = "Main Menu"),
	New_Menu 	UMETA(DisplayName = "New Menu"),
	Confirmation_Menu	UMETA(DisplayName = "Confirmation")
};

UCLASS()
class MYPROJECT_API AMyProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	/** Remove the current menu widget and create a new one from the specified class, if provided. */
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void ChangeMenuWidget(EMenuEnum MenuToDisplay);

	/** Remove the current menu widget*/
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void RemoveMenuWidgetFromViewport(EMenuEnum MenuToRemove);

	/** The widget classes which is used as menus */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UMG Menus")
	TMap<EMenuEnum, TSubclassOf<UUserWidget>> MenuMap;

	UFUNCTION(BlueprintCallable, Category = "UMG Game")
	void SetupMenus();

protected:

	/** The widget class we will use as our main menu when the game starts. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
	EMenuEnum StartingWidgetClass = EMenuEnum::Main_Menu;

	/** Menu widget instance */
	UPROPERTY(BlueprintReadOnly, Category = "UMG Menus")
	TMap<EMenuEnum, UUserWidget*> MenuInstances;

	UPROPERTY(BlueprintReadOnly, Category = "UMG Menus")
	TMap<EMenuEnum, bool> VisibleMenus;

	UPROPERTY(BlueprintReadWrite, Category = "TCP Networking")
	FString ListenerIP;

	UPROPERTY(BlueprintReadWrite, Category = "TCP Networking")
	int32 ListenerPort;
};
