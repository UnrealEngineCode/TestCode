// Fill out your copyright notice in the Description page of Project Settings.

#include "HostnameLookup.h"

HostnameLookup* HostnameLookup::Runnable = NULL;

HostnameLookup::HostnameLookup(uint32 *IpNumber, const FString* Name)
	: StopTaskCounter(0),
	IP(IpNumber),
	LookupHostName(Name)
{
	Thread = FRunnableThread::Create(this, TEXT("HostnameLookup"), 0U, EThreadPriority::TPri_Normal, FGenericPlatformAffinity::GetNoAffinityMask()); //windows default = 8mb for thread, could specify more
}

HostnameLookup::~HostnameLookup()
{
	delete Thread;
	Thread = NULL;
}

//Init
bool HostnameLookup::Init()
{
	//Init the Data 
	return true;
}

//Run
uint32 HostnameLookup::Run()
{
	//Initial wait before starting
	*IP = 0;
	uint32 OutIP = 0;
	int32 ErrorCode = 10;

	ISocketSubsystem* const SocketSubSystem = ISocketSubsystem::Get();

	if (SocketSubSystem)
	{

		auto ResolveInfo = SocketSubSystem->GetHostByName(TCHAR_TO_ANSI(*(*LookupHostName)));

		while (!ResolveInfo->IsComplete()) {
			FPlatformProcess::Sleep(0.1);
		}
		ErrorCode = ResolveInfo->GetErrorCode();
		if (ErrorCode == 0)
		{
			const FInternetAddr* Addr = &ResolveInfo->GetResolvedAddress();
			Addr->GetIp(OutIP);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("WAZAAAAAAPPPP is %d.%d.%d.%d: "), 0xff & (OutIP >> 24), 0xff & (OutIP >> 16), 0xff & (OutIP >> 8), 0xff & OutIP));
		}
	}
	*IP = OutIP;
	Stop();
	return ErrorCode;
}

//stop
void HostnameLookup::Stop()
{
	StopTaskCounter.Increment();
}

HostnameLookup* HostnameLookup::Lookup(uint32* IpNumber, const FString *Name)
{
	//Create new instance of thread if it does not exist
	//        and the platform supports multi threading!
	if (!Runnable && FPlatformProcess::SupportsMultithreading())
	{
		Runnable = new HostnameLookup(IpNumber, Name);
	}
	return Runnable;
}

void HostnameLookup::EnsureCompletion()
{
	Stop();
	Thread->WaitForCompletion();
}

void HostnameLookup::Shutdown()
{
	if (Runnable)
	{
		Runnable->EnsureCompletion();
		delete Runnable;
		Runnable = NULL;
	}
}

bool HostnameLookup::IsThreadFinished()
{
	if (Runnable) return Runnable->IsFinished();
	return true;
}

uint32 HostnameLookup::GetIP()
{
	if(IP) return *IP;
	return 0;
}

