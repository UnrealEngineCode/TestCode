// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "MyPlayerController.h"
#include "RotatingActor.h"

/**
*
*/

void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeGameAndUI());
}
