// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Networking.h"
#include "ImuTcpInterface.generated.h"

#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

class UHostNameTask;
class UEngine;
class UWorld;
class AActor;
class TCPSocket;
class TCPListenerSocket;

/**
 * 
 */

DECLARE_DELEGATE_OneParam(FHostnameLookup, FString);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnRecievedHostName, bool, Success, FString, IP);

UCLASS()
class MYPROJECT_API AImuTcpInterface : public AActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	AImuTcpInterface();

	virtual void Tick(float DeltaTime) override;

	TCPSocket* _Socket = NULL;

	FTimerHandle SocketReadTimerHandle;
	FTimerHandle ConnectionListenerTimerHandle;

	FHostnameLookup HostNameFunctor;

	//Timer functions, could be threads
	void Connect();
	void SetTimers();
	void ClearTimers();
	void DeleteSocket();
	void ReadSocketData();

	//Hostname resolver Delegate callback
	void ResolveHostNameCB(FString HostName);

	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnRecievedHostName onRecievedHostName;

	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		bool ConnectTCP(const FString IP, const int32 Port);

	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		bool DisconnectTCP();

	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		bool StartListenTCP(const FString IP, const int32 Port);

	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		void Send(FString message);

	// Asynchronously resolves HostName for IP and Hostnames
	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		void ResolveHostName(const FString HostName);

	// Recieves asynchronous events with resolved HostNames
	UFUNCTION(BlueprintImplementableEvent, Category = "TCP Networking")
		void RecievedHostName(const FString &IP);

	UFUNCTION(BlueprintImplementableEvent, Category = "TCP Networking")
		void RecievedData(const TArray<uint8> &BinaryArray);

	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		void StringFromBinaryArray(TArray<uint8> BinaryArray, bool& Success, FString& String);
	
	UFUNCTION(BlueprintCallable, Category = "TCP Networking")
		void FloatArrayFromBinaryArray(TArray<uint8> BinaryArray, bool& Success, TArray<float> &FloatArray);

	UFUNCTION()
		void DisplayMessage(bool Success, FString HostName);

};