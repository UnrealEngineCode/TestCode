#include "ImuTcpInterface.h"
#include "MyProject.h"

#include <string>
#include <algorithm>
#include "GameFramework/Actor.h"
#include "Engine.h"
#include "CoreMinimal.h"
#include "fireworks/Networking/include/HostNameTask.h"
#include "fireworks/Networking/include/TCPSocket.h"
#include "fireworks/Networking/include/TCPListenerSocket.h"

// Sets default values
AImuTcpInterface::AImuTcpInterface()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AImuTcpInterface::BeginPlay()
{
	this->onRecievedHostName.AddDynamic(this, &AImuTcpInterface::DisplayMessage);
	this->HostNameFunctor.BindUObject(this, &AImuTcpInterface::ResolveHostNameCB);
	Super::BeginPlay();
}

void AImuTcpInterface::DisplayMessage(bool Success, FString HostName)
{
	if (Success) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Lookup Completed Successfully")));
	}
}

void AImuTcpInterface::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorld()->GetTimerManager().ClearTimer(ConnectionListenerTimerHandle);
	GetWorld()->GetTimerManager().ClearTimer(SocketReadTimerHandle);

	DeleteSocket();
}

// Called every frame
void AImuTcpInterface::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//TCP connection
void AImuTcpInterface::Connect()
{
	_Socket->Connect();
}

void AImuTcpInterface::SetTimers()
{
	GetWorld()->GetTimerManager().SetTimer(ConnectionListenerTimerHandle, this, &AImuTcpInterface::Connect, 0.5f, true);
	GetWorld()->GetTimerManager().SetTimer(SocketReadTimerHandle, this, &AImuTcpInterface::ReadSocketData, 0.01f, true);
}

void AImuTcpInterface::ClearTimers()
{
	GetWorld()->GetTimerManager().ClearTimer(SocketReadTimerHandle);
	GetWorld()->GetTimerManager().ClearTimer(ConnectionListenerTimerHandle);
}

// TCP Server Code
bool AImuTcpInterface::StartListenTCP(const FString IP, const int32 Port)
{
	if (!_Socket) 
	{
		uint32 IPnbr = (uint32)FCString::Strtoui64(*IP, NULL, 10);
		_Socket = new TCPListenerSocket(IPnbr, Port, FString(TEXT("ImuSocketListener")));
		SetTimers();
	}
	if (_Socket)
	{
		return true;
	}
	return false;
}

// TCP Client Code
bool AImuTcpInterface::ConnectTCP(const FString IP, const int32 Port)
{
	if (!_Socket)
	{
		uint32 IPnbr = (uint32)FCString::Strtoui64(*IP, NULL, 10);
		_Socket = new TCPSocket(IPnbr, Port);
		SetTimers();
	}
	if (_Socket)
	{
		return true;
	}
	return false;
}

void AImuTcpInterface::DeleteSocket()
{
	if (_Socket != NULL)
	{
		_Socket->Close();
		delete _Socket;
		_Socket = NULL;
	}
}

bool AImuTcpInterface::DisconnectTCP()
{
	ClearTimers();
	DeleteSocket();
	return true;
}

void AImuTcpInterface::ReadSocketData()
{
	TArray<uint8> ReceivedData;
	ReceivedData.Empty();
	int32 len = -1;
	if (_Socket)
	{
		// read int32 defining the size of rest of the data
		_Socket->Read(ReceivedData, sizeof(int32));

		if (ReceivedData.Num() == sizeof(int32))
		{
			FMemory::Memcpy(&len, &ReceivedData.GetData()[0], sizeof(int32));
			ReceivedData.Empty();
			_Socket->Read(ReceivedData, len);
			if (ReceivedData.Num() == len)
			{
				RecievedData(ReceivedData);
			}
		}
	}
}

void AImuTcpInterface::Send(FString message) 
{
	if (_Socket)
	{
		_Socket->Write(message);
	}
}

//Asynchronous Callback
void AImuTcpInterface::ResolveHostNameCB(FString HostName)
{
	//Generates events
	RecievedHostName(HostName);
	
	//Broadcast to listeners
	onRecievedHostName.Broadcast(true, HostName);
}

void AImuTcpInterface::ResolveHostName(const FString HostName)
{
	(new FAutoDeleteAsyncTask<UHostNameTask>(HostNameFunctor, HostName))->StartBackgroundTask();
}

void AImuTcpInterface::StringFromBinaryArray(TArray<uint8> BinaryArray, bool& Success, FString& String)
{
	String = _Socket->StringFromBinaryArray(BinaryArray);
	Success = true;
}

void AImuTcpInterface::FloatArrayFromBinaryArray(TArray<uint8> BinaryArray, bool& Success, TArray<float> &FloatArray)
{
	FloatArray.AddUninitialized((int)(BinaryArray.Num() / sizeof(float)));
	FMemory::Memcpy(&FloatArray.GetData()[0], &BinaryArray.GetData()[0], BinaryArray.Num() * sizeof(uint8));
	Success = true;
}

