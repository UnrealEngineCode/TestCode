# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import socket
import struct
import time
import array

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.1.57", 64))

data = array.array('f', [0.0, 1.0, 2.0, 3.0])

databytes = data.tobytes()

binary_data = struct.pack('I%ds' % (len(databytes)), len(databytes), databytes)

while True:
    s.send(binary_data)
    data = s.recv(struct.calcsize("i"))
    length = struct.unpack("I", data)
    data = s.recv(int(length[0]))
    string = data.decode("utf-8")
    print(string)
    time.sleep(0.01)

s.close()
