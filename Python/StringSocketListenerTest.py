# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 20:35:48 2018
@author: Roger Isaksson
"""

import socket
import struct
import time

data = "Hello Unreal!"
bytestring = bytes(data, 'utf8')
binary_data = struct.pack("I%ds" % (len(bytestring)), len(bytestring), bytestring)

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(("192.168.1.57", 64))
serversocket.listen(5)

while 1:
    #wait to accept a connection - blocking call
    s, addr = serversocket.accept()
    print('Connected with ' + addr[0] + ':' + str(addr[1]))
    while 1:
         s.send(binary_data)
         data = s.recv(struct.calcsize("i"))
         length = struct.unpack("I", data)
         data = s.recv(int(length[0]))
         string = data.decode("utf-8")
         print(string)
         time.sleep(0.1)
