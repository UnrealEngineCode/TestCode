# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 20:35:48 2018
@author: Roger Isaksson
"""

import socket
import struct
import time
import array

data = array.array('f', [0.0, 1.0, 2.0, 3.0])
databytes = data.tobytes()
binary_data = struct.pack('I%ds' % (len(databytes)), len(databytes), databytes)

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(("192.168.1.57", 62))
serversocket.listen(5)

while 1:
    #wait to accept a connection - blocking call
    s, addr = serversocket.accept()
    print('Connected with ' + addr[0] + ':' + str(addr[1]))
    while 1:
         s.send(binary_data)
         data = s.recv(struct.calcsize("i"))
         length = struct.unpack("I", data)
         data = s.recv(int(length[0]))
         string = data.decode("utf-8")
         print(string)
         time.sleep(0.01)
